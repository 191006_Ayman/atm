/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import Exceptions.InvalidPinException;

/**
 *
 * @author mba
 */
public class ATMSimulator {
    public static void main(String args[]) {
        
        Bank bank = new Bank("Example");
        try{
            bank.addClient(new BankAccount("Ahmad", 2000, new Card(300, "0000")));
            
        }catch(InvalidPinException e){
            System.out.println(e.getMessage());
        }
        ATM atm = new ATM(10000);
        atm.addBank(bank);
        try{
            Card card = new Card(600, "0000");
            bank.addClient(new BankAccount("Samir", 5000, card));
            atm.readCard(card);
            atm.withdraw(200);
            atm.deposite(500);
        }catch(Exception e){
            System.out.println(e.getClass());
        }
        
        
        
        
    }
}
