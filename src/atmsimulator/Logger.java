/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author mba
 */
public class Logger implements OStream {
    private String file;

    public Logger(String file) {
        this.file = file;
    }

    @Override
    public void display(String str) {
        try{
            FileWriter fw = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fw);
            writer.write(str+"\n");
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        
    }
    
}
