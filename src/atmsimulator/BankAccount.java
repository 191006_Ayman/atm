/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import Exceptions.InsuffiecientBalanceException;
import Exceptions.NoEnoughtCashExeption;

/**
 *
 * @author mba
 */
public class BankAccount {
    private String name;
    private double balance;
    private Card card;

    public BankAccount(String name, double balance, Card card) {
        this.name = name;
        this.balance = balance;
        this.card = card;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public Card getCard() {
        return card;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCard(Card card) {
        this.card = card;
    }
    
    public void deposite(double amount){
        this.balance += amount;
    }
    
    public void withdraw(double amount) throws InsuffiecientBalanceException{
        if(amount > this.balance)
            throw new InsuffiecientBalanceException();
        else
            this.balance -= amount;
    }
    
    
}
