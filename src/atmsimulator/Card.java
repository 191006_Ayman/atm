/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import Exceptions.InvalidPinException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mba
 */
public class Card {
    private int no;
    private String pin;

    public Card() {
    }
    

    public Card(int no, String pin) throws InvalidPinException {
        Matcher m = Pattern.compile("\\d{4}").matcher(pin);
        if(!m.matches()){
            throw new InvalidPinException();
        }
        this.no = no;
        this.pin = pin;
    }

    public int getNo() {
        return no;
    }

    public String getPin() {
        return pin;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public void setPin(String pin) throws InvalidPinException {
        Matcher m = Pattern.compile("\\d{4}").matcher(pin);
        if(!m.matches()){
            throw new InvalidPinException();
        }
        this.pin = pin;
    }
    
}
