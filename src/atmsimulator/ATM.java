/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import Exceptions.CancelledException;
import Exceptions.InsuffiecientBalanceException;
import Exceptions.InvalidPinException;
import Exceptions.NoEnoughtCashExeption;
import java.util.ArrayList;

/**
 *
 * @author mba
 */
public class ATM {
    private ArrayList<Bank> banks = new ArrayList<Bank>();
    private String consoleText;
    private int state;
    private String logFile = "transactionlog.txt";
    private SystemReader reader;
    private Logger logger = new Logger(logFile);
    private double cash;
    
    private BankAccount currentBankAccount;

    public ATM(double cash) {
        this.cash = cash; 
        this.setReady();
    }
    
    public void addBank(Bank bank){
        this.banks.add(bank);
    }
    
    public void setReady(){
        this.consoleText = "Please Enter The Card:\nEnter card number\n";
        this.state = 1;
    }
    
    public void setCardInserted(){
        this.consoleText = "Please Enter The PIN\n";
        this.state = 2;
    }
    
    public void setSelectTransaction(){
        this.consoleText = "Please select a transaction:\nB1. Withdraw\nB2. Balance\n";
        this.state = 3;
    }
    
    public void setWithdraw(){
        this.consoleText = "Please Enter The amount you want to withdraw\n";
        this.state = 4;
    }
    
    public void setDispense(){
        this.consoleText = "Please Collect the money and Exit\n";
        this.state = 5;
    }
    
    public void setDisplayBalance(){
        this.consoleText = this.currentBankAccount.getBalance() + "\nB1. Back\n";
        this.state = 6;
    }
    
    public void setErrorMessage(String message){
        this.consoleText = message;
        this.state = 7;
       
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public String getConsoleText() {
        return consoleText;
    }

    public int getState() {
        return state;
    }

    public BankAccount getCurrentBankAccount() {
        return currentBankAccount;
    }
    
    public void readCard(Card card) throws InvalidPinException{
        for(Bank bank: banks){
            BankAccount b = bank.searchClient(card);
            if(!(bank.searchClient(card) == null)){
                if(!b.getCard().getPin().equals(card.getPin()))
                    throw new InvalidPinException();
                this.currentBankAccount = bank.searchClient(card);
            }
        }
        if(this.currentBankAccount == null)
            this.setErrorMessage("Account Not Found!");
    }
    
    public void deposite(double amount){
        this.currentBankAccount.deposite(amount);
        this.cash += amount;
        String str = "Clien: "+this.currentBankAccount.getName()+"\tdeposite: "+amount+"\tnew Balance: "+this.currentBankAccount.getBalance()+"\n";
        logger.display(str);
        System.out.println(str);
    }
    
    public void withdraw(double amount) throws NoEnoughtCashExeption{
        if(amount > this.cash)
            throw new NoEnoughtCashExeption();
        this.cash -= amount;
    }
    
    public void cancel() throws CancelledException{
        throw new CancelledException();
    }

    public void setCurrentBankAccount(BankAccount currentBankAccount) {
        this.currentBankAccount = currentBankAccount;
    }
    
    
    
}
