/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import java.util.Scanner;

/**
 *
 * @author mba
 */
public class SystemReader implements IStream {
    private Scanner sc = new Scanner(System.in);
    @Override
    public String readStr() {
        return sc.next();
    }

    @Override
    public int readInt() {
        return sc.nextInt();
    }

    @Override
    public double readdouble() {
        return sc.nextDouble();
    }
    
}
