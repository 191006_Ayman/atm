/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsimulator;

import java.util.ArrayList;

/**
 *
 * @author mba
 */
public class Bank {
    private String name;
    private ArrayList<BankAccount> clients;

    public Bank(String name) {
        this.name = name;
        clients = new ArrayList<BankAccount>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<BankAccount> getClients() {
        return clients;
    }
    
    public void addClient(BankAccount client){
        this.clients.add(client);
    }
    
    public BankAccount searchClient(Card card){
        for(BankAccount b: this.clients){
            if(card.getNo() == b.getCard().getNo())
                return b;
        }
        return null;
    }
}
