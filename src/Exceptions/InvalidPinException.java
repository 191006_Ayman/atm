/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author mba
 */
public class InvalidPinException extends Exception {

    public InvalidPinException() {
        super("Invalid PIN, PIN should be 4 Digit Numbers");
    }
    
}
